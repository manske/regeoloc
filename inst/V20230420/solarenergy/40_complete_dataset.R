library(regeoloc)
library(dplyr)
library(tidyr)
library(sf)

# load data
pv_area <- sf::st_read("inst/V20230420/solarenergy/32_update_area_QGIS.geojson")
pv_data <- sf::st_read("inst/V20230420/solarenergy/32_update_dataset_QGIS.geojson")

# complete pv_area
R_area <- pv_area %>%
  sf::st_transform(., crs = 3035) %>%
  dplyr::mutate(.,
                area_size = round(as.numeric(sf::st_area(.)) / 10000, 4),
                area_accuracy = ifelse(is.na(area_accuracy), 1, area_accuracy)) %>%
  dplyr::arrange(., area_size) %>%
  sf::st_transform(., crs = 4326) %>%
  dplyr::mutate(.,
                area_id = 1:nrow(.),
                x_coordinates = st_coordinates(st_point_on_surface(.))[,1],
                y_coordinates = st_coordinates(st_point_on_surface(.))[,2]) %>%
  dplyr::select(., area_id, area_size, area_accuracy, x_coordinates, y_coordinates) %>%
  sf::st_make_valid(.)

# complete pv_data
sf_use_s2(FALSE)
R_data <- pv_data %>%
  dplyr::filter(., is.na(classification )) %>%
  dplyr::select(., -x_coordinates, -y_coordinates) %>%
  sf::st_join(., R_area) %>%
  sf::st_drop_geometry(.) %>%
  dplyr::select(., energy_source:slope, area_id, x_coordinates, y_coordinates, location_accuracy, note:reference_date) %>%
  sf::st_as_sf(., coords = c("x_coordinates", "y_coordinates"), crs = 4326, remove = FALSE) %>%
  dplyr::mutate(., note = NA) %>%
  dplyr::arrange(., commissioning_date, installed_capacity)

# related pv_data
R_area$area_related <- ifelse(R_area$area_id %in% R_data$area_id, 1, 0)
R_area <- R_area %>%  dplyr::select(., area_id, area_size, area_accuracy, area_related, x_coordinates, y_coordinates)

# save data
sf::st_write(R_area, "inst/V20230420/solarenergy/41_complete_area.geojson", delete_dsn = FALSE)
sf::st_write(R_data, "inst/V20230420/solarenergy/41_complete_dataset.geojson", delete_dsn = FALSE)

