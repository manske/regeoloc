#' Lists available data set versions
#'
#' @return A data frame containing the available data set versions.
#' @export
#'
#' @examples
#' show_versions()
show_versions <- function() {

  versions <- list.files(system.file(package = "regeoloc"), pattern = "^V.*")
  path <- unname(sapply(versions, function(x) system.file(x, package = "regeoloc")))
  df <- data.frame(Version = versions, Path = path, Index = 1:length(versions))
  print(df)

  x <- readline(prompt = "Enter index to browse a version or press enter to skip: ")
  if (x <= nrow(df)) {
    x <- as.numeric(x)
    browseURL(df[x,2])
  } else {
    stop("Index entered does not exist")
  }

}




