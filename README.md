
<!-- README.md is generated from README.Rmd. Please edit that file -->

# regeoloc

<!-- badges: start -->
<!-- badges: end -->

*regeoloc* documents the creation of a data set of wind power plants, PV
field systems, bioenergy plants and hydropower plants for Germany based
on a data extract from the [Core Energy Market Data Register
(CEMDR)](https://www.marktstammdatenregister.de/MaStR).

The final data set as documented by *regeoloc* can be found under
<https://doi.org/10.5281/zenodo.6922043>.

## Installation

*regeoloc* can be installed as follows:

``` r
# install.packages("remotes")
library(remotes)
remotes::install_git("https://git.ufz.de/manske/regeoloc.git")
```

## Structure

The package is organised to contain different versions of the data set
processing. Each version is documented in several sequential files by
renewable energy source. There are .R scripts, .geojson and .csv files
documenting the data set creation process.

To browse the versions, use `regeoloc::show_versions()`.
